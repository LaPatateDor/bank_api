openapi: 3.0.1
info:
  title: Banking API
  description: 'This is the documentation for the Webservice IT exercice. It will
    detail the routes and models for a Banking API '
  contact:
    email: nicolas.gerard@campus.academy
  version: 1.2.0
externalDocs:
  description: Link to the Git Project
  url: https://gitlab.com/LaPatateDor/bank_api/-/tree/master
servers:
- url: /v1
tags:
- name: User
  description: Actions regarding the user
- name: Account
  description: Actions regarding the account
- name: Transfer
  description: Deposit and withdrawal
paths:
  /user:
    get:
      tags:
      - User
      summary: Get all users
      description: Returns all users
      operationId: getUser
      responses:
        200:
          description: successful operation
          content:
            application/xml:
              schema:
                $ref: '#/components/schemas/User'
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: User not found
          content: {}
      security:
      - api_key: []
    put:
      tags:
      - User
      summary: Update a user account
      description: This route is used to modify a field or multiple fields in a user
        account
      operationId: updateUser
      requestBody:
        description: User object that needs to be added to the store
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
          application/xml:
            schema:
              $ref: '#/components/schemas/User'
        required: true
      responses:
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: User not found
          content: {}
        405:
          description: Validation exception
          content: {}
      security:
      - user_auth:
        - write:user
        - read:user
      x-codegen-request-body-name: body
    post:
      tags:
      - User
      summary: Account creation, add a user
      description: This route is used to create an account and add it to our database
      operationId: addUser
      requestBody:
        description: User informations
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
          application/xml:
            schema:
              $ref: '#/components/schemas/User'
        required: true
      responses:
        405:
          description: Invalid /format
          content: {}
        406:
          description: User already exist
          content: {}
      security:
      - user_auth:
        - write:user
        - read:user
      x-codegen-request-body-name: body
  /user/{userId}:
    get:
      tags:
      - User
      summary: Find user by ID
      description: Returns a single user
      operationId: getUserById
      parameters:
      - name: userId
        in: path
        description: ID of user to return
        required: true
        schema:
          type: string
      responses:
        200:
          description: successful operation
          content:
            application/xml:
              schema:
                $ref: '#/components/schemas/User'
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: User not found
          content: {}
      security:
      - api_key: []
    delete:
      tags:
      - User
      summary: Deletes a User
      operationId: deleteUser
      parameters:
      - name: api_key
        in: header
        schema:
          type: string
      - name: userId
        in: path
        description: User id to delete
        required: true
        schema:
          type: integer
          format: int64
      responses:
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: User not found
          content: {}
      security:
      - user_auth:
        - write:user
        - read:user
  /Account:
    get:
      tags:
      - Account
      summary: Get all accounts
      description: Returns all accounts
      operationId: Get account
      responses:
        200:
          description: successful operation
          content:
            application/xml:
              schema:
                $ref: '#/components/schemas/Account'
            application/json:
              schema:
                $ref: '#/components/schemas/Account'
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: Account not found
          content: {}
      security:
      - api_key: []
    put:
      tags:
      - Account
      summary: Update an account
      description: This route is used to modify a field or multiple fields in an account
      operationId: updateAccount
      requestBody:
        description: Account object that needs to be added to the Bank
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Account'
          application/xml:
            schema:
              $ref: '#/components/schemas/Account'
        required: true
      responses:
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: Account not found
          content: {}
        405:
          description: Validation exception
          content: {}
      security:
      - user_auth:
        - write:account
        - read:account
      x-codegen-request-body-name: body
    post:
      tags:
      - Account
      summary: Account creation
      description: This route is used to create an account and add it to our database
      operationId: addAccount
      requestBody:
        description: Account informations
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Account'
          application/xml:
            schema:
              $ref: '#/components/schemas/Account'
        required: true
      responses:
        405:
          description: Invalid /format
          content: {}
        406:
          description: Account already exist
          content: {}
      security:
      - user_auth:
        - write:user
        - read:user
      x-codegen-request-body-name: body
  /account/{accountId}:
    get:
      tags:
      - Account
      summary: Find account by ID
      description: Returns a single account
      operationId: getAccountByID
      parameters:
      - name: accountId
        in: path
        description: ID of account to return
        required: true
        schema:
          type: string
      responses:
        200:
          description: successful operation
          content:
            application/xml:
              schema:
                $ref: '#/components/schemas/Account'
            application/json:
              schema:
                $ref: '#/components/schemas/Account'
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: Account not found
          content: {}
      security:
      - api_key: []
    delete:
      tags:
      - Account
      summary: Deletes an Account
      operationId: deleteAccount
      parameters:
      - name: api_key
        in: header
        schema:
          type: string
      - name: accountId
        in: path
        description: Account id to delete
        required: true
        schema:
          type: integer
          format: int64
      responses:
        400:
          description: Invalid ID supplied
          content: {}
        404:
          description: Account not found
          content: {}
      security:
      - user_auth:
        - write:account
        - read:account
  /transfer:
    post:
      tags:
      - Transfer
      summary: Virement banquaire
      description: This route is used to transfer money from an account to another
      operationId: Paiement
      requestBody:
        description: Transfer informations
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Transfer'
          application/xml:
            schema:
              $ref: '#/components/schemas/Transfer'
        required: true
      responses:
        405:
          description: Invalid /format
          content: {}
        406:
          description: Transfer already exist
          content: {}
      security:
      - user_auth:
        - write:transfer
        - read:transfer
      x-codegen-request-body-name: body
components:
  schemas:
    Account:
      type: object
      properties:
        id:
          type: integer
          format: int64
        AccountNumber:
          type: string
        operations:
          type: string
        Balance:
          type: integer
          
      xml:
        name: Category
    User:
      type: object
      properties:
        id:
          type: integer
          format: int64
        accountNumber:
          type: string
        firstName:
          type: string
        lastName:
          type: string
        birthdate:
          type: string
          format: date-time
        civility:
          type: string
          description: Civility
          enum:
          - Madam
          - Sir
          - Other
        email:
          type: string
        adress:
          type: string
        password:
          type: string
        phone:
          type: string
        userStatus:
          type: integer
          description: User Status
          format: int32
      xml:
        name: User
    Transfer:
      type: object
      properties:
        id:
          type: integer
          format: int64
        destination_account:
          type: string
        sender_account:
          type: string
        ammount:
          type: integer
          format: int64
      xml:
        name: Tag
  securitySchemes:
    user_auth:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: http://petstore.swagger.io/oauth/dialog
          scopes:
            write:user: modify users in your account
            read:user: read your uers
    api_key:
      type: apiKey
      name: api_key
      in: header
