// Adding all of the requires we will use
const express = require("express");
const app = express();
const path = require("path");
const http = require("http").createServer(app);
const fs = require("fs");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const httpRequest = require('http');

// httpRequest.get("http://localhost:8080/auth/realms/BankApi", (resp) => {
//   var str = "";
//   resp.on('data', function (chunk) {
//     str += chunk;
//   });

//   resp.on('end', function () {
//     console.log(str);
//   });
// })

// Joining the different js module parts of the armor
const account = require(path.join(__dirname, "/Account/account.js"));
const user = require(path.join(__dirname, "/User/user.js"));
const transfer = require(path.join(__dirname, "/Transfer/transfer.js"));

// Adding the IHM access allow
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//Setting up the database connection
mongoose.connect("mongodb://127.0.0.1:27017/Bank", {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", function() {
  console.log("Connected to the database");
});

//Declaring the static dirname of our project
app.use(express.static(path.join(__dirname, "/")));

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// Setting the different routes for all of the armor parts and for the armor
app.use("/user", user);
app.use("/account", account.router);
app.use("/transfer", transfer);

// Default get when no routes are called
app.get("/", (req, res) => {
  res.send("Voici la page d'accueil");
});

http.listen(3000);
