// Initializing all of the requires we need
var express = require("express");
var router = express.Router();
const fs = require("fs");
const mongoose = require("mongoose");
const random = require('random');
const jwt = require("jsonwebtoken");
const {
  Encode
} = require("../JwtAuth/encodage");
const {
  Decode
} = require("../JwtAuth/decodage");

//Getting the user from the JSON file
var userExemple = fs.readFileSync("User/user.json");
var user = JSON.parse(userExemple);

//Mongoose Database create
var userschema = mongoose.Schema({
  accountNumber: String,
  firstName: String,
  lastName: String,
  birthdate: Date,
  civility: String,
  email: String,
  adress: String,
  password: String,
  phone: String,
  userStatus: Number,
});

var users = mongoose.model("Users", userschema);

//inserting into the database if the document doesn't exist
users.exists({
  users
}, (err, res) => {
  if (res) {
    console.log("The database is already Populated by Users, no changes done");
  } else {
    users.insertMany(user, (err, res) => {
      console.log(res);
    });
  }
});

//Route to get a list of all of the users
router.get("/", (req, res) => {
  Decode(req.headers.authorization);
  users.find(function (err, users) {
    if (err) {
      res.send(err);
    }
    res.json(users);
  });
});

//Route to get a list of all of the users
router.get("/:id", (req, res) => {
  users.findById(req.params.id, function (err, users) {
    if (err) {
      res.send(err);
    }
    res.json(users);
  });
});

//Route to get a list of all of the users
router.post("/Signin", (req, res) => {

  var Payload = {
    "iss": req.body.issuer,
    "sub": req.body.subject,
    "aud": req.body.audience,
    "name": req.body.name,
    "admin": req.body.admin,
    "iat": req.body.iat,
    "exp": req.body.exp,
  };

  var signature = Encode(Payload);
  res.send(signature);

});

//Route to add a new user to the Database
router.post("/", (req, res) => {
  var randomer = random.int(0, 9).toString();
  for (let i = 0; i < 7; i++) {
    randomer = randomer + random.int(0, 9);
  }
  var accountNumberRandom = "A77777" + randomer;
  var userCreate = new users({
    accountNumber: accountNumberRandom,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    birthdate: req.body.birthdate,
    civility: req.body.civility,
    email: req.body.email,
    adress: req.body.adress,
    password: req.body.password,
    phone: req.body.phone,
    userStatus: req.body.userStatus,
  });
  users.find({
    accountNumber: accountNumberRandom
  }, (err, res) => {
    if (err) {
      console.log("AH!");
    } else {
      console.log("Super du coup")
    }
  })
  userCreate.save(function (err) {
    if (err) {
      res.send(err);
    }
    res.send(accountNumberRandom + " added to the Database :\n" + userCreate);
  });
});

//Route to delete an user from its id
router.delete("/:id", (req, res) => {
  users.findOneAndDelete(req.params.id, (err, res) => {
    console.log(req.params.id);
  });
  res.send("Coucou world");
});

//route to update a user
router.put("/:id", (req, res) => {
  var userUpdate = {
    accountNumber: req.body.accountNumber,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    birthdate: req.body.birthdate,
    civility: req.body.civility,
    email: req.body.email,
    adress: req.body.adress,
    password: req.body.password,
    phone: req.body.phone,
    userStatus: req.body.userStatus,
  };
  users.findOneAndUpdate(req.params.id, userUpdate, (err, res) => {
    console.log(userUpdate);
  });
  res.send(userUpdate);
});

module.exports = router;