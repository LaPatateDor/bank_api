// Initializing all of the requires we need
var express = require("express");
var router = express.Router();
const fs = require("fs");
const mongoose = require("mongoose");
const {
  Encode
} = require("../JwtAuth/encodage");
const {
  Decode
} = require("../JwtAuth/decodage");

const accountJS = require("../Account/account.js");

//Route to get a list of all of the accounts
router.get("/", (req, res) => {
  res.send("He he he, YUP")
});

//Route to add a new account to the Database
router.post("/checkToken", (req, res) => {
  
  const isValid = Decode(JSON.stringify(req.headers.authorization));
  res.send("Yup" + isValid);

});

//Route to get a token
router.post("/tokenGen", (req, res) => {

const request = require('request');
request.post(
 {
    url: 'http://localhost:8080/auth/realms/BankApi/protocol/openid-connect/token',
    form: {
      client_id:"BanqueNicolas",
      client_secret:"e106f2c8-b93b-462d-a99f-bf7dd93ef6d2",
      grant_type:"client_credentials",
      scope:"openid"}
  },
  (error, response, body) => {
    console.log("TOKEN : " + JSON.parse(body).access_token);
    res.send(JSON.parse(body).access_token);
  }
)  
});

//Route to do a transfer
router.post("/", (req, res) => {
  const isValid = Decode(req.headers.authorization);
  if (isValid) {
    accountJS.account.findOne({
      accountNumber: req.body.source_account
    }, (err, succes) => {
      if (err) {
        res.send(err);
      }
      var accountupdate = succes;
      accountupdate.balance += req.body.amount;
      accountupdate.save(function (err, doc) {
        if (err) return console.error(err);
        console.log("Document inserted succussfully!");
      });
    })

    accountJS.account.findOne({
      accountNumber: req.body.destination_account
    }, (err, succes) => {
      if (err) {
        res.send(err);
      }
      var accountupdate = succes;
      accountupdate.balance -= req.body.amount;
      accountupdate.save(function (err, doc) {
        if (err) return console.error(err);
        console.log("Document inserted succussfully!");
      });
    })
  }
  res.send("Cool");
});

module.exports = router;