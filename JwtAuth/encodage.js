const base64 = require('base64url');
var fs = require("fs");
var crypto = require("crypto");
const signatureFunction = crypto.createSign('RSA-SHA256');

function EncodageJWT(payload){
var Header = 
    {
        "alg": "RS256",
        "typ": "JWT"
      };

var Payload = payload;

var headerString = JSON.stringify(Header);

var payloadString = JSON.stringify(Payload);

const headerBase64 = base64(headerString);
const payloadBase64 = base64(payloadString);

signatureFunction.write(headerBase64 + '.' + payloadBase64);
signatureFunction.end();

const PRIV_KEY = fs.readFileSync(__dirname + '/jwtRS256.key', 'utf8');

const signatureBase64 = signatureFunction.sign(PRIV_KEY, 'base64');

const signatureBase64Url = base64.fromBase64(signatureBase64);

var signature = headerBase64+"."+payloadBase64+"."+signatureBase64Url;

return signature;
//module.exports = { Jwt: signature };
}

module.exports = {Encode : EncodageJWT};
