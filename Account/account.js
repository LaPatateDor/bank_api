// Initializing all of the requires we need
var express = require("express");
var router = express.Router();
const fs = require("fs");
const mongoose = require("mongoose");
const random = require('random');

//Getting the account from the JSON file
var accountExemple = fs.readFileSync("Account/account.json");
var account = JSON.parse(accountExemple);

//Mongoose Database create
var accountschema = mongoose.Schema({
  accountNumber: String,
  balance: Number,
});

var accounts = mongoose.model("Accounts", accountschema);

//inserting into the database if the document doesn't exist
accounts.exists({
  accounts
}, (err, res) => {
  if (res) {
    console.log("The database is already Populated by accounts, no changes done");
  } else {
    accounts.insertMany(account, (err, res) => {
      console.log(res);
    });
  }
});

//Route to get a list of all of the accounts
router.get("/", (req, res) => {
  accounts.find(function (err, accounts) {
    if (err) {
      res.send(err);
    }
    res.json(accounts);
  });
});

//Route to get a list of all of the accounts
router.get("/:id", (req, res) => {
  accounts.findById(req.params.id, function (err, accounts) {
    if (err) {
      res.send(err);
    }
    res.json(accounts);
  });
});

//Route to add a new account to the Database
router.post("/", (req, res) => {
  var randomer = random.int(0, 9).toString();
  for (let i = 0; i < 7; i++) {
    randomer = randomer + random.int(0, 9);
  }
  var accountNumberRandom = "A77777" + randomer;
  var accountCreate = new accounts({
    accountNumber: accountNumberRandom,
    balance: req.body.balance
  });
  accounts.find({
    accountNumber: accountNumberRandom
  }, (err, res) => {
    if (err) {
      console.log("AH!");
    } else {
      console.log("Super du coup")
    }
  })
  accountCreate.save(function (err) {
    if (err) {
      res.send(err);
    }
    res.send(accountNumberRandom + " added to the Database :\n" + accountCreate);
  });
});

//Route to delete an account from its id
router.delete("/:number", (req, res) => {
  accounts.findOneAndDelete({
    accountNumber: req.params.number
  }, (err, res) => {
    console.log(req.params.number);
  });
  res.send("Coucou world");
});

//route to update an account
router.put("/:id", (req, res) => {
  var accountUpdate = {
    balance: req.body.balance
  };
  accounts.findOneAndUpdate(req.params.id, accountUpdate, (err, res) => {
    console.log(accountUpdate);
  });
  res.send(accountUpdate);
});

module.exports = {router, account : accounts};